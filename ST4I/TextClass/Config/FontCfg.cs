﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace ST4I.TextClass.Config
{
    class FontCfg
    {
        public ObservableCollection<string> ListFonts { get; set; }
        public Range<int> FontSize { get; set; }
        public Range<double> Opacity { get; set; }
        public Range<int> LetterSpacing { get; set; }
        public Range<int> LineSpacing { get; set; }
        public FontStyle Style { get; set; }
        public FontCfg()
        {
            ListFonts = new ObservableCollection<string>();
            FontSize = new Range<int>(12,15, 1, 999);
            Opacity = new Range<double>(0.5,1, 0, 1);
            LetterSpacing = new Range<int>(0,5, -999, 999);
            LineSpacing = new Range<int>(0, 5,- 999, 999);
            Style = FontStyle.Regular;
        }
    }
    
}
