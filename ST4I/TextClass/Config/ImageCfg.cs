﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Collections.ObjectModel;


namespace ST4I.TextClass.Config
{
    class ImageCfg
    {
        public int NumOfImage 
        { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public Rotate Rotation { get; set; }
        public bool SetBGFromImage { get; set; }
        public Color BackgroundColor { get; set; }
        public ObservableCollection<string> ListBGImagePath { get; set; }
        public ImageCfg()
        {
            ListBGImagePath = new ObservableCollection<string>();
            NumOfImage = 10;
            Width = 100;
            Height = 100;
            BackgroundColor = Color.FromArgb(255, 255, 255);
            Rotation = new Rotate();
        }
    }
    
}
