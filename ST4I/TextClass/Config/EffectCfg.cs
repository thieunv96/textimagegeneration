﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ST4I.TextClass.Config
{
    class EffectCfg
    {
        public bool RandomTextPosition { get; set; }
        public Range<int> RandomTextPositionXRange { get; set; }
        public Range<int> RandomTextPositionYRange { get; set; }
        public bool AddAverageBlur { get; set; }
        public Range<int> AverageBlurRange { get; set; }
        public bool AddMedianBlur { get; set; }
        public Range<int> MedianBlurRange { get; set; }
        public bool AddGaussianBlur { get; set; }
        public Range<int> GaussianBlurRange { get; set; }
        public bool AddPepperNoise { get; set; }
        public Range<double> PepperNoiseRange { get; set; }
        public EffectCfg()
        {
            AverageBlurRange = new Range<int>(1,5,1,255);
            MedianBlurRange = new Range<int>(1,5, 1, 255);
            GaussianBlurRange = new Range<int>(1,5, 1, 255);
            PepperNoiseRange = new Range<double>(1,5, 0, 100);
            RandomTextPositionXRange = new Range<int>(0,10,-999,999);
            RandomTextPositionYRange = new Range<int>(0, 10, -999, 999);
        }
    }
}
