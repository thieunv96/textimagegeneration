﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ST4I.TextClass.Config
{
    class OutputCfg
    {
        public string DirSave { get; set; }
        public bool WriteLocationFile { get; set; }
        public bool ClearFile { get; set; }
        public NameFormat FileNameFormat { get; set; }
        public LabelFileFormat LabelFormat { get; set; }
        public OutputCfg()
        {
            DirSave = "C:/Test";
            WriteLocationFile = true;
            ClearFile = true;
        }
    }
}
