﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.Structure;
using Newtonsoft.Json;
using System.Xml.Serialization;

namespace ST4I.TextClass.Config
{
    public class GResult
    {
        public Image<Bgr, byte> TextImage { get; set; }
        public List<CharLabel> Labels { get; set; }
        public GResult()
        {
            Labels = new List<CharLabel>();
        }
        public string ToJson()
        {
            lock(this)
            {
                ImageInfo info = new ImageInfo();
                string content = string.Empty;
                foreach (var item in Labels)
                {
                    content += item.Data;
                    info.Chars.Add(item);
                }
                info.Content = content;
                return JsonConvert.SerializeObject(info);
            }
            
        }
        public string ToXml()
        {
            lock(this)
            {
                ImageInfo info = new ImageInfo();
                string content = string.Empty;
                foreach (var item in Labels)
                {
                    content += item.Data;
                    info.Chars.Add(item);
                }
                info.Content = content;
                using (var stringwriter = new System.IO.StringWriter())
                {
                    var serializer = new XmlSerializer(info.GetType());
                    serializer.Serialize(stringwriter, info);
                    return stringwriter.ToString();
                }
            }
            
        }
        public string ToTextArray()
        {
            lock(this)
            {
                string data = string.Empty;
                string content = string.Empty;
                foreach (var item in Labels)
                {
                    content += item.Data;
                    if (data != string.Empty)
                        data += "\n";
                    data += $"((({item.Box.X},{item.Box.Y}), ({item.Box.X + item.Box.Width},{item.Box.Y}), ({item.Box.X + item.Box.Width},{item.Box.Y + item.Box.Height}), ({item.Box.X},{item.Box.Y + item.Box.Height})),{item.Data})";
                }
                return data;
            }
            
        }
    }
    public class CharLabel
    {
        public Rectangle Box { get; set; }
        public char Data { get; set; }
    }
    public class ImageInfo
    {
        public string Content { get; set; }
        public List<CharLabel> Chars { get; set; }
        public ImageInfo()
        {
            Chars = new List<CharLabel>();
        }
    }
}
