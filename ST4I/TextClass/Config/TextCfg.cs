﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace ST4I.TextClass.Config
{
    class TextCfg
    {
        public bool RandomText { get; set; }
        public Range<int> RandomTextLength { get; set; }
        public Range<int> RandomTextLine { get; set; }
        public bool OtherText { get; set; }
        public bool Text09 { get; set; }
        public bool TextAZUpper { get; set; }
        public bool TextAZLower { get; set; }
        public string OtherTextData { get; set; }
        public string ImportTextData { get; set; }
        public string TextFilePath { get; set; }
        public Color TextColor { get; set; }
        public TextCfg()
        {
            RandomText = true;
            RandomTextLength = new Range<int>(5,10,1,999); 
            RandomTextLine= new Range<int>(1,2,1,999);
            Text09 = true;
            TextAZUpper = false;
            TextAZLower = false;
            ImportTextData = string.Empty;
            TextFilePath = string.Empty;
            OtherTextData = string.Empty;
            TextColor = Color.FromArgb(0, 0, 0);
        }
    }
}
