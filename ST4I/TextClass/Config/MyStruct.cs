﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ST4I.TextClass.Config
{
    class Range<T>
    {
        private bool _Loaded { get; set; }
        private T _Min { get; set; }
        private T _Max { get; set; }
        public T Max 
        {
            get => _Max; 
            set 
            { 
                _Max = value;
                if (_Loaded)
                    CheckCondition();
            }
        }
        public T Min
        {
            get => _Min;
            set
            {
                _Min = value;
                if(_Loaded)
                    CheckCondition();
            }
        }
        public T Maximum { get; set; }
        public T Minimum { get; set; }
        public Range()
        {
            _Loaded = true;
        }
        public Range(T min, T max, T minimum, T maximum)
        {
            Max = max;
            Min = min;
            Minimum = minimum;
            Maximum = maximum;
            _Loaded = true;
        }
        private void CheckCondition()
        {
            double max = Convert.ToDouble(Max);
            double min = Convert.ToDouble(Min);
            double maximum = Convert.ToDouble(Maximum);
            double minimum = Convert.ToDouble(Minimum);
            if (min < minimum)
            {
                Min = Minimum;
            }
            if(min > maximum)
            {
                Min = Maximum;
            }
            if(max > maximum)
            {
                Max = Maximum;
            }
            if(max <minimum)
            {
                Max = Minimum;
            }
            if(min > max)
            {
                Max = Min;
            }
        }
        public object GetRandomValue()
        {
            Type typeParameterType = typeof(T);
            Random random = new Random();
            if (typeParameterType.Name == "Double")
            {
                double max = Convert.ToDouble(Max);
                double min = Convert.ToDouble(Min);
                max = max > min ? max : min;
                double value = random.Next(Convert.ToInt32(min * 100), Convert.ToInt32(max * 100)) / 100.0;
                return value;
            }
            else if(typeParameterType.Name == "Int32")
            {
                int max = Convert.ToInt32(Max);
                int min = Convert.ToInt32(Min);
                max = max > min ? max : min;
                int value = random.Next(min, max);
                return value;
            }
            return null;
        }
    }
    
    class Rotate
    {
        public Range<double> X { get; set; }
        public Range<double> Y { get; set; }
        public Range<double> Z { get; set; }
        public Rotate()
        {
            X = new Range<double>(0,0,-45,45);
            Y = new Range<double>(0, 0, -45, 45);
            Z = new Range<double>(0, 0, -180, 80);
        }
    }
    enum FontStyle
    {
        Regular,
        Bold,
        Italic,
        Underline
    }
    enum NameFormat
    {
        ByIndex,
        ByTime,
    }
    enum LabelFileFormat
    {
        Json,
        Xml,
        TextArray, 
        Tesseract,
    }
    enum EffectType
    {
        AverageBlur,
        MedianBlur,
        GaussianBlur,
        PepperNoise
    }
}
