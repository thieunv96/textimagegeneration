﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
namespace ST4I.TextClass.Config
{
    class GClass
    {
        private static Object _Lock = new Object();
        private static GClass _Instance = null;
        public static GClass GetInstance()
        {
            lock(_Lock)
            {
                if (_Instance == null)
                {
                    string path = Properties.Settings.Default.SaveConfig;

                    _Instance = new GClass();
                    if (File.Exists(path))
                    {
                        string s = File.ReadAllText(path);
                        try
                        {
                            _Instance = JsonConvert.DeserializeObject<GClass>(s);
                            
                        }
                        catch
                        {
                        }
                    }
                }
            }
            return _Instance;
        }
        public void Save()
        {
            string path = Properties.Settings.Default.SaveConfig;
            FileInfo fi = new FileInfo(path);
            if (!Directory.Exists(fi.Directory.FullName))
            {
                Directory.CreateDirectory(fi.Directory.FullName);
            }
            File.WriteAllText(path, JsonConvert.SerializeObject(this));
        }
        public FontCfg Font { get; set; }
        public ImageCfg Image { get; set; }
        public TextCfg Text { get; set; }
        public EffectCfg Effect { get; set; }
        public OutputCfg Output { get; set; }
        public GClass()
        {
            Font = new FontCfg();
            Image = new ImageCfg();
            Text = new TextCfg();
            Effect = new EffectCfg();
            Output = new OutputCfg();
        }
    }
    
}
