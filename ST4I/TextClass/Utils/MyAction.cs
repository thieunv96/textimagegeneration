﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Text;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System.IO;
using Newtonsoft.Json;

namespace ST4I.TextClass.Utils
{
    class MyAction
    {
        private static PrivateFontCollection mFontCollection = new PrivateFontCollection();
        public static double XAngle { get; set; }
        public static double YAngle { get; set; }
        public static double ZAngle { get; set; }
        public static Config.GResult Create(Config.GClass Gconfig, int Index)
        {
            Gconfig.Save();
            Config.GResult gResult = null;
            Image<Bgr, byte> imageInit = InitImage(Gconfig.Image);
            int imgw = Gconfig.Image.Width;
            int imgh = Gconfig.Image.Height;
            Rectangle ROIInit = new Rectangle(imageInit.Width / 2 - imgw / 2, imageInit.Height / 2 - imgh / 2, imgw, imgh);
            imageInit.ROI = ROIInit;
            Image<Bgr, byte> imageOutput = imageInit.Copy();
            List<Rectangle> listCharBox = new List<Rectangle>();
            List<char> listChar = new List<char>();
            if (imageOutput != null)
            {
                 Random random = new Random();
                int xSt = 0;
                int ySt = 0;
                if (Gconfig.Effect.RandomTextPosition)
                {
                    xSt = (int)Gconfig.Effect.RandomTextPositionXRange.GetRandomValue();
                    ySt = (int)Gconfig.Effect.RandomTextPositionYRange.GetRandomValue();
                }
                int numLine = (int)Gconfig.Text.RandomTextLine.GetRandomValue();
                Rectangle ROISt = new Rectangle(xSt, ySt, imageOutput.Width - xSt, imageOutput.Height - ySt);
                imageOutput.ROI = ROISt;
                Font font = GetFont(Gconfig.Font.ListFonts.ToArray(), Gconfig.Font.Style, Gconfig.Font.FontSize);
                using (Image<Bgr, byte> image = imageOutput.Copy())
                {
                    if (font != null)
                    {
                        int heightChar = GetHeightOneChar(font);
                        string[] content = GetArrayContent(Gconfig.Text, heightChar, (int)Gconfig.Image.Height);
                        Utils.DrawCharacter draw = new DrawCharacter();
                        int y = (int)(0.2 * heightChar);
                        for (int row = 0; row < content.Length; row++)
                        {
                            if (Gconfig.Text.RandomText)
                                if (row > numLine - 1 || y > image.Height)
                                    break;
                            int x = 0;
                            for (int col = 0; col < content[row].Length; col++)
                            {
                                string chr = content[row][col].ToString();
                                DrawResult result = draw.DrawText(chr, font, Gconfig.Text.TextColor);
                                Rectangle ROI = new Rectangle(x, y, result.Image.Width, result.Image.Height);
                                if (ROI.X >= image.Width || ROI.Y >= image.Height)
                                    break;
                                if (ROI.X < 0 || ROI.Y < 0)
                                    continue;
                                image.ROI = ROI;
                                // tinh toan ROI anh render 
                                // tinh toan char Box
                                bool isOutImage = false;
                                Rectangle charBox = result.CharBox;
                                if (image.Width < result.Image.Width || image.Height < result.Image.Height)
                                {
                                    Rectangle ROIResult = new Rectangle(0, 0, image.Width, image.Height);
                                    result.Image.ROI = ROIResult;
                                    result.Mask.ROI = ROIResult;
                                    if (charBox.X + charBox.Width > ROIResult.Width || charBox.Height + charBox.Y > ROIResult.Height)
                                    {
                                        isOutImage = true;
                                    }
                                }
                                if (!isOutImage)
                                {
                                    charBox.X += x + xSt;
                                    charBox.Y += y + ySt;
                                    listCharBox.Add(charBox);
                                    listChar.Add(chr[0]);
                                }
                                
                                double opacity = (double)Gconfig.Font.Opacity.GetRandomValue();
                                using (Image<Bgr, byte> imageAdd = new Image<Bgr, byte>(result.Image.Size))
                                using (Image<Bgr, byte> imageOpacity = new Image<Bgr, byte>(result.Image.Size))
                                using (Image<Bgr, byte> imageMaskInv = new Image<Bgr, byte>(result.Mask.Size))
                                {
                                    CvInvoke.BitwiseNot(result.Mask, imageMaskInv);
                                    CvInvoke.AddWeighted(image, 1 - opacity, result.Image, opacity, 1, imageOpacity);
                                    CvInvoke.BitwiseAnd(imageOpacity, imageMaskInv, imageOpacity);
                                    CvInvoke.BitwiseAnd(image, result.Mask, image);
                                    CvInvoke.Add(imageOpacity, image, imageAdd);
                                    imageAdd.CopyTo(image);
                                }


                                image.ROI = Rectangle.Empty;
                                x += result.Image.Width;
                                if (x >= image.Width)
                                    break;
                                int letterSpaing = (int)Gconfig.Font.LetterSpacing.GetRandomValue();
                                x += letterSpaing;
                                result.Dispose();
                            }
                            y += (int)(heightChar + (0.2 * heightChar));
                            int lineSpacing = (int)Gconfig.Font.LineSpacing.GetRandomValue();
                            y += lineSpacing;
                        }
                    }
                    image.CopyTo(imageOutput);
                    
                }

                imageOutput.ROI = Rectangle.Empty;
                imageOutput.CopyTo(imageInit);
                imageInit.ROI = Rectangle.Empty;
                imageInit = ApplyRotate(imageInit);
                imageOutput.Dispose();
                imageInit.ROI = ROIInit;
                imageOutput = imageInit.Copy();
                imageInit.Dispose();
                AddEffect(imageOutput, Gconfig.Effect);
                gResult = new Config.GResult();
                gResult.TextImage = imageOutput;
                for (int i = 0; i < listCharBox.Count; i++)
                {
                    Config.CharLabel charLabel = new Config.CharLabel();
                    charLabel.Box = listCharBox[i];
                    charLabel.Data = listChar[i];
                    gResult.Labels.Add(charLabel);
                }
            }
            return gResult;
        }
        public static Image<Bgr, byte> ApplyRotate(Image<Bgr, byte> image)
        {
            ST4I.Utils.CvUtils.ImageRotation(image, new Point(image.Width / 2, image.Height / 2), ZAngle * Math.PI / 180.0);
            return image;
        }
        public static void AddEffect(Image<Bgr, byte> image, Config.EffectCfg effectSt)
        {
            List<Config.EffectType> listEffect = new List<Config.EffectType>();
            if (effectSt.AddAverageBlur)
                listEffect.Add(Config.EffectType.AverageBlur);
            if (effectSt.AddMedianBlur)
                listEffect.Add(Config.EffectType.MedianBlur);
            if (effectSt.AddGaussianBlur)
                listEffect.Add(Config.EffectType.GaussianBlur);
            if (effectSt.AddPepperNoise)
                listEffect.Add(Config.EffectType.PepperNoise);
            if (listEffect.Count > 0)
            {
                Random random = new Random();
                int id = random.Next(0, listEffect.Count);
                int k = 0;
                switch (listEffect[id])
                {
                    case Config.EffectType.AverageBlur:
                        k = (int)effectSt.AverageBlurRange.GetRandomValue();
                        CvInvoke.Blur(image, image, new Size(k, k), new Point(-1, -1), Emgu.CV.CvEnum.BorderType.Default);
                        break;
                    case Config.EffectType.MedianBlur:
                        k = (int)effectSt.MedianBlurRange.GetRandomValue();
                        k = k % 2 == 0 ? k + 1 : k;
                        CvInvoke.MedianBlur(image, image, k);
                        break;
                    case Config.EffectType.GaussianBlur:
                        k = (int)effectSt.GaussianBlurRange.GetRandomValue();
                        k = k % 2 == 0 ? k + 1 : k;
                        CvInvoke.GaussianBlur(image, image, new Size(k, k), 0);
                        break;
                    case Config.EffectType.PepperNoise:
                        double percent = (double)effectSt.PepperNoiseRange.GetRandomValue();
                        int num = Convert.ToInt32((percent * (double)(image.Width * image.Height)) / 100);
                        for (int i = 0; i < num; i++)
                        {
                            int x = random.Next(0, image.Width);
                            int y = random.Next(0, image.Height);
                            int val = random.Next(0, 256);
                            val = val > 127 ? 255 : 0;
                            image[y, x] = new Bgr(val, val, val);
                        }
                        break;
                    default:
                        break;
                }

            }
        }
        public static int GetHeightOneChar(Font font)
        {
            SizeF textSize = new SizeF(1, 1);
            using (Image img = new Bitmap(1, 1))
            using (Graphics temp = Graphics.FromImage(img))
            {
                textSize = temp.MeasureString("A", font);
            }
            return (int)textSize.Width;
        }
        public static string[] GetArrayContent(Config.TextCfg textSt, int heightChar, int heightImage)
        {
            List<string> content = new List<string>();
            double inflateHeightChar = (heightChar + 0.2 * heightChar);
            int num = heightImage / (int)inflateHeightChar;
            if (textSt.RandomText)
            {
                string listChar = string.Empty;
                if (textSt.Text09)
                {
                    for (int i = 48; i < 58; i++)
                    {
                        listChar += (char)i;
                    }
                }
                if (textSt.TextAZUpper)
                {
                    for (int i = 65; i < 91; i++)
                    {
                        listChar += (char)i;
                    }
                }
                if (textSt.TextAZLower)
                {
                    for (int i = 97; i < 123; i++)
                    {
                        listChar += (char)i;
                    }
                }
                if (textSt.OtherText && textSt.OtherTextData != null)
                {
                    listChar += textSt.OtherTextData;
                }
                listChar.Replace(" ", "");
                Random random = new Random();
                if (listChar.Length > 0)
                {
                    for (int i = 0; i < num; i++)
                    {
                        string str = string.Empty;
                        int length = (int)textSt.RandomTextLength.GetRandomValue();
                        for (int c = 0; c < length; c++)
                        {
                            int idChar = random.Next(0, listChar.Length);
                            str += listChar[idChar];
                        }
                        content.Add(str);
                    }
                }

            }
            else
            {
                if (!string.IsNullOrEmpty(textSt.ImportTextData))
                {
                    string text = textSt.ImportTextData;
                    content.AddRange(text.Split('\n'));
                }
            }
            return content.ToArray();
        }
        public static Font GetFont(string[] ListFont, Config.FontStyle FontStyle, Config.Range<int> FontSize)
        {
            Font font = null;
            FontStyle fontStyle = System.Drawing.FontStyle.Regular;
            switch (FontStyle)
            {
                case Config.FontStyle.Regular:
                    fontStyle = System.Drawing.FontStyle.Regular;
                    break;
                case Config.FontStyle.Bold:
                    fontStyle = System.Drawing.FontStyle.Bold;
                    break;
                case Config.FontStyle.Italic:
                    fontStyle = System.Drawing.FontStyle.Italic;
                    break;
                case Config.FontStyle.Underline:
                    fontStyle = System.Drawing.FontStyle.Underline;
                    break;
                default:
                    break;
            }
            if (ListFont.Length > 0)
            {
                Random random = new Random();
                int id = random.Next(0, ListFont.Length);
                if (mFontCollection != null)
                {
                    mFontCollection.Dispose();
                    mFontCollection = null;
                }
                mFontCollection = new PrivateFontCollection();
                mFontCollection.AddFontFile(ListFont[id]);
                using (FontFamily fontFamily = new FontFamily(mFontCollection.Families[0].Name, mFontCollection))
                {
                    int fontSize = random.Next(Convert.ToInt32(FontSize.Min * 10), Convert.ToInt32((FontSize.Max) * 10));
                    font = new Font(fontFamily, (float)(fontSize) / 10, fontStyle);
                }

            }
            return font;
        }
        public static Image<Bgr, byte> InitImage(Config.ImageCfg imageSt)
        {
            Image<Bgr, byte> image = null;
            
            Size imageSize = GetImageSize(imageSt);
            int width = imageSize.Width;
            int height = imageSize.Height;
            if (!imageSt.SetBGFromImage)
            {
                image = new Image<Bgr, byte>(width, height, new Bgr(imageSt.BackgroundColor.B, imageSt.BackgroundColor.G, imageSt.BackgroundColor.R));
            }
            else if (imageSt.SetBGFromImage)
            {
                image = new Image<Bgr, byte>(width, height);
                List<string> bgFilePath = new List<string>();
                bgFilePath.AddRange(imageSt.ListBGImagePath);
                if (bgFilePath.Count > 0)
                {
                    Random random = new Random();
                    int id = random.Next(0, bgFilePath.Count);
                    using (Image<Bgr, byte> imgBg = new Image<Bgr, byte>(bgFilePath[id]))
                    {
                        int stepw = image.Width % imgBg.Width == 0 ? image.Width / imgBg.Width : image.Width / imgBg.Width + 1;
                        int steph = image.Height % imgBg.Height == 0 ? image.Height / imgBg.Height : image.Height / imgBg.Height + 1;
                        for (int x = 0; x < stepw; x++)
                        {
                            for (int y = 0; y < steph; y++)
                            {
                                Rectangle ROIDrawImage = new Rectangle(new Point(x * imgBg.Width, y * imgBg.Height), imgBg.Size);
                                image.ROI = ROIDrawImage;
                                if (image.Width < imgBg.Width || image.Height < imgBg.Height)
                                {
                                    imgBg.ROI = new Rectangle(new Point(0, 0), image.Size);
                                }
                                imgBg.CopyTo(image);
                                imgBg.ROI = Rectangle.Empty;
                                image.ROI = Rectangle.Empty;
                            }
                        }
                    }
                }
            }
            return image;
        }
        public static Size GetImageSize(Config.ImageCfg imageSt)
        {
            int width = imageSt.Width;
            int height = imageSt.Height;
            Size imageSize = new Size(width, height);
            ZAngle = (double)imageSt.Rotation.Z.GetRandomValue();
            // z rotate
            Size zSize = GetZRotate(ZAngle, width, height);
            // x rotate
            XAngle = (double)imageSt.Rotation.X.GetRandomValue();
            int xh = GetXYRotate(XAngle, height);
            // y rotate
            YAngle = (double)imageSt.Rotation.X.GetRandomValue();
            int yw = GetXYRotate(XAngle, width);
            imageSize = new Size(Math.Max(zSize.Width, yw), Math.Max(zSize.Height, xh));
            return imageSize;
        }
        private static Size GetZRotate(double angle, int width, int height)
        {
            Point[] v = ST4I.Utils.CvUtils.GetVertices(new Rectangle(0, 0, width, height));
            double zAngle = angle;
            for (int i = 0; i < v.Length; i++)
            {
                v[i] = ST4I.Utils.CvUtils.PointRotation(v[i], new Point(width / 2, height / 2), zAngle * Math.PI / 180.0);
            }
            var zXSort = v.OrderBy(z => z.X);
            var zYSort = v.OrderBy(z => z.Y);
            int zw = Math.Abs(zXSort.ElementAt(0).X - zXSort.ElementAt(3).X);
            int zh = Math.Abs(zYSort.ElementAt(0).Y - zYSort.ElementAt(3).Y);
            return new Size(zw, zh);
        }
        private static int GetXYRotate(double angle, int a)
        {
            int x = Convert.ToInt32((1 / Math.Cos(angle * Math.PI / 180.0)) * a * 2);
            return x;
        }
    }
}
