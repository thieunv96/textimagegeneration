﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System.Diagnostics;

namespace ST4I.Utils
{
    class CvUtils
    {
        public static Point PointRotation(Point RotatePoint, Point Center, double Angle)
        {
            int x = RotatePoint.X - Center.X;
            int y = RotatePoint.Y - Center.Y;
            int x1 = (int)Math.Round(x * Math.Cos(Angle) - y * Math.Sin(Angle));
            int y1 = (int)Math.Round(x * Math.Sin(Angle) + y * Math.Cos(Angle));
            RotatePoint.X = x1 + Center.X;
            RotatePoint.Y = y1 + Center.Y;
            return RotatePoint;
        }
        public static Image<Bgr, byte> ImageRotation(Image<Bgr, byte> scr, Point Center, double Angle)
        {
            Angle = Angle * 180.0 / Math.PI;
            using (Mat rotMatrix = new Mat())
            {
                CvInvoke.GetRotationMatrix2D(Center, -Angle, 1, rotMatrix);
                CvInvoke.WarpAffine(scr, scr, rotMatrix, scr.Size);
            }
            return scr;
        }
        public static Image<Gray, byte> ImageRotation(Image<Gray, byte> scr, Point Center, double Angle)
        {
            Angle = Angle * 180.0 / Math.PI;
            using (Mat rotMatrix = new Mat())
            {
                CvInvoke.GetRotationMatrix2D(Center, -Angle, 1, rotMatrix);
                CvInvoke.WarpAffine(scr, scr, rotMatrix, scr.Size);
            }
            return scr;
        }
        public static Image<Bgr, byte> HighlightInRect(Image<Bgr, byte> src, Rectangle rect)
        {
            Image<Bgr, byte> result = null;
            using (VectorOfVectorOfPoint contoursInRect = new VectorOfVectorOfPoint())
            using (Image<Gray, byte> imageGray = src.Convert<Gray, byte>())
            using (VectorOfVectorOfPoint contoursOutRect = new VectorOfVectorOfPoint())
            {
                imageGray.ROI = rect;
                CvInvoke.FindContours(imageGray, contoursInRect, null, Emgu.CV.CvEnum.RetrType.Ccomp, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple);
                imageGray.ROI = Rectangle.Empty;
                CvInvoke.Rectangle(imageGray, rect, new MCvScalar(0), -1);
                CvInvoke.FindContours(imageGray, contoursOutRect, null, Emgu.CV.CvEnum.RetrType.Ccomp, Emgu.CV.CvEnum.ChainApproxMethod.ChainApproxSimple);
                Size imageSize = src.Size;
                src.Dispose();
                src = null;
                result = new Image<Bgr, byte>(imageSize);
                result.ROI = rect;
                CvInvoke.DrawContours(result, contoursInRect, -1, new MCvScalar(0,0,255), -1);
                result.ROI = Rectangle.Empty;
                CvInvoke.DrawContours(result, contoursOutRect, -1, new MCvScalar(50,50,50), -1);
            }
            GC.Collect();
            GC.WaitForPendingFinalizers();
            return result;
        }
        public static Image<Bgr, byte> ImageTransformation(Image<Bgr, byte> scr, int X, int Y)
        {
              float[,] translationArray = { { 1, 0, X }, { 0, 1, Y } };
            using (Matrix<float> translationMatrix = new Matrix<float>(translationArray))
            {
                CvInvoke.WarpAffine(scr, scr, translationMatrix, scr.Size);
            }
            return scr;
        }
        public static Image<Gray, byte> ImageTransformation(Image<Gray, byte> scr, int X, int Y)
        {
            float[,] translationArray = { { 1, 0, X }, { 0, 1, Y } };
            using (Matrix<float> translationMatrix = new Matrix<float>(translationArray))
            {
                CvInvoke.WarpAffine(scr, scr, translationMatrix, scr.Size);
            }
            return scr;
        }
        public static Point[] GetVertices(Rectangle R)
        {
            Point[] v = new Point[4];
            v[0] = new Point(R.X, R.Y);
            v[1] = new Point(R.X + R.Width , R.Y);
            v[2] = new Point(R.X + R.Width, R.Y + R.Height);
            v[3] = new Point(R.X, R.Y + R.Height);
            return v;
        }
        public static int GetCircleByThreePoint(Point P1, Point P2, Point P3, ref PointF Center, ref double Radius)
        {
            // phuong trinh duong thang p1, p2
            float dt12a = (P1.X - P2.X) == 0 ? 0 : (P1.Y - P2.Y) / (P1.X - P2.X);
            float dt12b = P2.Y - (dt12a * P2.X);
            // phuon trinh duong thang p2,p3
            float dt23a = (P3.X - P2.X) == 0 ? 0 : (P3.Y - P2.Y) / (P3.X - P2.X);
            float dt23b = P2.Y - (dt12a * P2.X);
            //tim trung diem
            PointF mid12 = new PointF((P1.X + P2.X) / 2, (P1.Y + P2.Y) / 2);
            PointF mid23 = new PointF((P3.X + P2.X) / 2, (P3.Y + P2.Y) / 2);
            //tim duong thang vuong goc di qua trung dien
            dt12a = dt12a == 0 ? 0 : -1 / dt12a;
            dt12b = mid12.Y - dt12a * mid12.X;
            dt23a = dt23a == 0 ? 0 : -1 / dt23a;
            dt23b = mid23.Y - dt23a * mid23.X;
            // tinh tam va ban kinh
            Center.X = (dt12a - dt23a) == 0 ? 0 : ((dt23b - dt12b) / (dt12a - dt23a));
            Center.Y = (dt23a * Center.X + dt23b);
            Radius = (float)Math.Sqrt(Math.Pow((P1.X - Center.X), 2) + Math.Pow((P1.Y - Center.Y), 2));
            return 0;
        }
        public static double DistanceTwoPoint(Point P1, Point P2)
        {
            return Math.Sqrt(Math.Pow(P2.X - P1.X, 2) + Math.Pow(P2.Y - P1.Y, 2));
        }
        public static double DistanceSquaredAngle(Point P1, Point P2)
        {
            System.Windows.Vector v1 = new System.Windows.Vector(P2.X - P1.X, P2.Y - P1.Y);
            System.Windows.Vector vx = new System.Windows.Vector(1, 0);
            System.Windows.Vector vy = new System.Windows.Vector(0, 1);
            double angle = System.Windows.Vector.AngleBetween(v1, vx);
            if (Math.Abs(angle % 90) > 45)
            {
                angle = System.Windows.Vector.AngleBetween(v1, vy);
            }
            angle = Math.Abs(angle % 90);
            double leght = DistanceTwoPoint(P1, P2);
            double dist = Math.Cos(angle * Math.PI / 180.0) * leght;
            return dist;
        }
        public static double ContourArea(Point[] contour)
        {
            double s = 0;
            int npoint = contour.Length;
            var sortX = contour.OrderBy(item => item.X);
            var sortY = contour.OrderBy(item => item.Y);
            int x = sortX.ElementAt(0).X;
            int y = sortY.ElementAt(0).Y;
            int w = sortX.ElementAt(npoint - 1).X - sortX.ElementAt(0).X  + 1;
            int h = sortY.ElementAt(npoint - 1).Y - sortY.ElementAt(0).Y + 1;
            Point[] subPoint = new Point[npoint];
            for (int i = 0; i < npoint; i++)
            {
                subPoint[i] = new Point(contour[i].X - x, contour[i].Y - y);
            }
            using (Image<Gray, byte> image = new Image<Gray, byte>(w, h))
            using (VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint())
            {
                contours.Push(new VectorOfPoint(subPoint));
                CvInvoke.DrawContours(image, contours, -1, new MCvScalar(255), -1);
                s = CvInvoke.CountNonZero(image);
            }
            return s;
        }
        public static double ContourArea(VectorOfPoint contour)
        {
            return ContourArea(contour.ToArray());
        }
        public static bool IntersectsContour(Point[] Cnt1, Point[] Cnt2)
        {
            bool intersect = false;
            bool canIntersect = false;
            Point[] cntPTranform1 = new Point[Cnt1.Length];
            Point[] cntPTranform2 = new Point[Cnt2.Length];
            Rectangle BoudingCnt1 = CvInvoke.BoundingRectangle(new VectorOfPoint(Cnt1));
            for (int i = 0; i < Cnt1.Length; i++)
            {
                cntPTranform1[i] = new Point(Cnt1[i].X - BoudingCnt1.X, Cnt1[i].Y - BoudingCnt1.Y);
            }
            for (int i = 0; i < Cnt2.Length; i++)
            {
                cntPTranform2[i] = new Point(Cnt2[i].X - BoudingCnt1.X, Cnt2[i].Y - BoudingCnt1.Y);
                if ((cntPTranform2[i].X > 0 && cntPTranform2[i].X < BoudingCnt1.Width) ||
                    (cntPTranform2[i].Y > 0 && cntPTranform2[i].Y < BoudingCnt1.Height))
                    canIntersect = true;
            }
            if (canIntersect)
            {
                using (VectorOfPoint cnt1 = new VectorOfPoint(cntPTranform1))
                using (VectorOfPoint cnt2 = new VectorOfPoint(cntPTranform2))
                using (VectorOfVectorOfPoint contour1 = new VectorOfVectorOfPoint())
                using (VectorOfVectorOfPoint contour2 = new VectorOfVectorOfPoint())
                using (Image<Gray, byte> image1 = new Image<Gray, byte>(BoudingCnt1.Size))
                using (Image<Gray, byte> image2 = new Image<Gray, byte>(BoudingCnt1.Size))
                using (Image<Gray, byte> imageAnd = new Image<Gray, byte>(BoudingCnt1.Size))
                {
                    contour1.Push(cnt1);
                    contour2.Push(cnt2);
                    CvInvoke.DrawContours(image1, contour1, -1, new MCvScalar(255), -1);
                    CvInvoke.DrawContours(image2, contour2, -1, new MCvScalar(255), -1);
                    CvInvoke.BitwiseAnd(image1, image2, imageAnd);
                    int count = CvInvoke.CountNonZero(imageAnd);
                    if (count > 0)
                    {
                        intersect = true;
                    }
                }
            }
            return intersect;
        }
    }
}
