﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ST4I.Views.TextGenerator.MyPage
{
    /// <summary>
    /// Interaction logic for PText.xaml
    /// </summary>
    public partial class PText : Page
    {
        public PText()
        {
            InitializeComponent();
            frPreview.Navigate(new PPreview());
            rbRandomText.IsChecked = rbRandomText.IsChecked;
            InputText.IsChecked = InputText.IsChecked;
        }
        private void rbRandomText_Checked(object sender, RoutedEventArgs e)
        {
            if (frTextMode != null)
                frTextMode.NavigationService.Navigate(new PRandomText());
        }

        private void InputText_Checked(object sender, RoutedEventArgs e)
        {
            if(frTextMode != null)
                frTextMode.NavigationService.Navigate(new PInputText());
        }
    }
}
