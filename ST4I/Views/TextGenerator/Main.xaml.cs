﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ST4I.Resources.Theme;
using System.Diagnostics;

namespace ST4I.Views.TextGenerator
{
    /// <summary>
    /// Interaction logic for Main.xaml
    /// </summary>
    public partial class Main : Window
    {
		public Main()
        {
            ThemesController.ThemeTypes theme = Properties.Settings.Default.Theme == 0 ? 
                ThemesController.ThemeTypes.Dark : ThemesController.ThemeTypes.Light;
            ThemesController.SetTheme(theme);
            InitializeComponent();
            frMain.NavigationService.Navigate(new MyPage.PStart());
		}

        private void Hyperlink_RequestNavigate(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }

        private void self_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            foreach (Window win in Application.Current.Windows)
            {
                if(win != this)
                win.Close();
            }
        }
    }
}
