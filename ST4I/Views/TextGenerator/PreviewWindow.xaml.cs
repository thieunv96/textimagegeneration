﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ST4I.TextClass.Config;
using ST4I.TextClass.Utils;
using System.ComponentModel;
using System.Globalization;
using System.Threading;

namespace ST4I.Views.TextGenerator
{
    /// <summary>
    /// Interaction logic for PreviewWindow.xaml
    /// </summary>
    public partial class PreviewWindow : Window
    {
        private GClass _Config = GClass.GetInstance(); 
        public PreviewWindow()
        {
            InitializeComponent();
            Thread t = new Thread(() => {
                Thread.Sleep(200);
                this.Dispatcher.Invoke(() => {

                    btRefresh_Click(null, null);
                });
            });
            t.Start();
        }
        private void btZoomIn_Click(object sender, RoutedEventArgs e)
        {
            double zoom = imb.ZoomScale * 1.2;
            zoom = zoom <= imb.MaxZoomScale ? zoom : imb.MaxZoomScale;
            imb.ZoomScale = zoom;
        }

        private void btFill_Click(object sender, RoutedEventArgs e)
        {
            if(imb.Source != null)
            imb.FillScreen(); 
        }

        private void ZoomScale_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Slider slider = sender as Slider;
            slider.Value = Convert.ToInt32(slider.Value);
        }

        private void btRefresh_Click(object sender, RoutedEventArgs e)
        {
            var result = MyAction.Create(_Config, 0);
            if (imb != null)
            {
                bool initScale = imb.Source == null;
                imb.SourceFromBitmap = result.TextImage.Bitmap;
                if (initScale)
                    imb.ZoomScale = 1;
            }
        }

        private void btZoomOut_Click(object sender, RoutedEventArgs e)
        {
            double zoom = imb.ZoomScale * 0.8;
            zoom = zoom >= imb.MinZoomScale ? zoom : imb.MinZoomScale;
            imb.ZoomScale = zoom;
        }
    }
    
}
