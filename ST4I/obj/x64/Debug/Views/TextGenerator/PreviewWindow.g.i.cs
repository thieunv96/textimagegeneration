﻿#pragma checksum "..\..\..\..\..\Views\TextGenerator\PreviewWindow.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "4921DE0939DFFD2ADFF69D03420FBC0E3338D75CD7269BE82087B0AB4FD7A6D8"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Heal.MyControl;
using ST4I.Views.TextGenerator;
using ST4I.ViewsModel.Converter;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace ST4I.Views.TextGenerator {
    
    
    /// <summary>
    /// PreviewWindow
    /// </summary>
    public partial class PreviewWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 42 "..\..\..\..\..\Views\TextGenerator\PreviewWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Slider ZoomScale;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\..\..\..\Views\TextGenerator\PreviewWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btZoomIn;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\..\..\..\Views\TextGenerator\PreviewWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btZoomOut;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\..\..\..\Views\TextGenerator\PreviewWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btFill;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\..\..\..\Views\TextGenerator\PreviewWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btRefresh;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\..\..\..\Views\TextGenerator\PreviewWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Heal.MyControl.ImageBox imb;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ST4I;component/views/textgenerator/previewwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\Views\TextGenerator\PreviewWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.ZoomScale = ((System.Windows.Controls.Slider)(target));
            
            #line 43 "..\..\..\..\..\Views\TextGenerator\PreviewWindow.xaml"
            this.ZoomScale.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<double>(this.ZoomScale_ValueChanged);
            
            #line default
            #line hidden
            return;
            case 2:
            this.btZoomIn = ((System.Windows.Controls.Button)(target));
            
            #line 48 "..\..\..\..\..\Views\TextGenerator\PreviewWindow.xaml"
            this.btZoomIn.Click += new System.Windows.RoutedEventHandler(this.btZoomIn_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.btZoomOut = ((System.Windows.Controls.Button)(target));
            
            #line 53 "..\..\..\..\..\Views\TextGenerator\PreviewWindow.xaml"
            this.btZoomOut.Click += new System.Windows.RoutedEventHandler(this.btZoomOut_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.btFill = ((System.Windows.Controls.Button)(target));
            
            #line 58 "..\..\..\..\..\Views\TextGenerator\PreviewWindow.xaml"
            this.btFill.Click += new System.Windows.RoutedEventHandler(this.btFill_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.btRefresh = ((System.Windows.Controls.Button)(target));
            
            #line 65 "..\..\..\..\..\Views\TextGenerator\PreviewWindow.xaml"
            this.btRefresh.Click += new System.Windows.RoutedEventHandler(this.btRefresh_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.imb = ((Heal.MyControl.ImageBox)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

