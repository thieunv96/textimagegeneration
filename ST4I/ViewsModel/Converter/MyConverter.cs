﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Data;
using System.Globalization;
using ST4I.TextClass.Config;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows;

namespace ST4I.ViewsModel.Converter
{
    class String2EnumFonStyle : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int fontId = System.Convert.ToInt32(value);
            return fontId;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            TextClass.Config.FontStyle fontStyle = (TextClass.Config.FontStyle)value;
            return fontStyle;
        }
    }
    class String2EnumNameFormat: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int fontId = System.Convert.ToInt32(value);
            return fontId;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            TextClass.Config.NameFormat fontStyle = (TextClass.Config.NameFormat)value;
            return fontStyle;
        }
    }
    class String2EnumLabelFormat : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int fontId = System.Convert.ToInt32(value);
            return fontId;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            TextClass.Config.LabelFileFormat fontStyle = (TextClass.Config.LabelFileFormat)value;
            return fontStyle;
        }
    }
    public class BrushColorCvt : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            System.Drawing.Color? color = value as System.Drawing.Color?;
            SolidColorBrush brush = new SolidColorBrush(Color.FromRgb(color.Value.R, color.Value.G, color.Value.B));
            return brush;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            SolidColorBrush brush = value as SolidColorBrush;
            return System.Drawing.Color.FromArgb(brush.Color.R, brush.Color.G, brush.Color.B);
        }

    }
    public class Bool2Visiblity : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((bool)value == true)
            {
                return Visibility.Visible;
            }
            else
            {
                return Visibility.Hidden;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((Visibility)value == Visibility.Hidden)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
    class ScalePrecent : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((double)value * 100);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return ((double)value / 100);
        }
    }
}
