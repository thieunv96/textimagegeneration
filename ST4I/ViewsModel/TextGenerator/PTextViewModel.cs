﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ST4I.Utils;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using ST4I.Views.TextGenerator.MyPage;
using ST4I.TextClass.Config;
using form = System.Windows.Forms;
using System.IO;

namespace ST4I.ViewsModel.TextGenerator
{
    class PTextViewModel : PropertyChangedBase
    {
        public ICommand NextClickCmd { get; set; }
        public ICommand BackClickCmd { get; set; }
        public ICommand BrowserClickCmd { get; set; }
        public ICommand ColorClickCmd { get; set; }
        public  GClass _Config = GClass.GetInstance();
        public int RandomTextLengthMax
        {
            get => _Config.Text.RandomTextLength.Max;
            set
            {
                _Config.Text.RandomTextLength.Max = value;
                OnPropertyChanged();
            }
        }
        public int RandomTextLengthMin
        {
            get => _Config.Text.RandomTextLength.Min;
            set
            {
                _Config.Text.RandomTextLength.Min = value;
                OnPropertyChanged();
            }
        }
        public int RandomTextLineMax
        {
            get => _Config.Text.RandomTextLine.Max;
            set
            {
                _Config.Text.RandomTextLine.Max = value;
                OnPropertyChanged();
            }
        }
        public int RandomTextLineMin
        {
            get => _Config.Text.RandomTextLine.Min;
            set
            {
                _Config.Text.RandomTextLine.Min = value;
                OnPropertyChanged();
            }
        }
        public System.Drawing.Color TextColor
        {
            get => _Config.Text.TextColor;
            set
            {
                _Config.Text.TextColor = value;
                OnPropertyChanged();
            }
        }
        public bool RandomText
        {
            get => _Config.Text.RandomText;
            set
            {
                _Config.Text.RandomText = value;
                OnPropertyChanged();
            }
        }
        public bool InputText
        {
            get => !_Config.Text.RandomText;
            set
            {
                _Config.Text.RandomText = !value;
                OnPropertyChanged();
            }
        }
        public bool Text09
        {
            get => _Config.Text.Text09;
            set
            {
                _Config.Text.Text09 = value;
                OnPropertyChanged();
            }
        }
        public bool TextAZLower
        {
            get => _Config.Text.TextAZLower;
            set
            {
                _Config.Text.TextAZLower = value;
                OnPropertyChanged();
            }
        }
        public bool TextAZUpper
        {
            get => _Config.Text.TextAZUpper;
            set
            {
                _Config.Text.TextAZUpper = value;
                OnPropertyChanged();
            }
        }
        public bool OtherText
        {
            get => _Config.Text.OtherText;
            set
            {
                _Config.Text.OtherText = value;
                OnPropertyChanged();
            }
        }
        public string OtherTextData
        {
            get => _Config.Text.OtherTextData;
            set
            {
                _Config.Text.OtherTextData = value;
                OnPropertyChanged();
            }
        }
        public string ImportTextData
        {
            get => _Config.Text.ImportTextData;
            set
            {
                _Config.Text.ImportTextData = value;
                OnPropertyChanged();
            }
        }
        public string TextFilePath
        {
            get => _Config.Text.TextFilePath;
            set
            {
                if(!string.IsNullOrEmpty(value))

                    if(File.Exists(value))
                    {
                        ImportTextData += File.ReadAllText(value);
                        _Config.Text.TextFilePath = value;
                        OnPropertyChanged();
                    }
            }
        }
        public PTextViewModel()
        {
            NextClickCmd = new RelayCommand<object>((p) => { return true; }, (p) => { NextClick(p); });
            BackClickCmd = new RelayCommand<object>((p) => { return true; }, (p) => { BackClick(p); });
            BrowserClickCmd = new RelayCommand<object>((p) => { return true; }, (p) => { BrowserClick(p); });
            ColorClickCmd = new RelayCommand<object>((p) => { return true; }, (p) => { ColorSelectionChanged(); });
        }
        private void NextClick(object p)
        {
            if(p != null)
            {
                Page np = p as Page;
                np.NavigationService.Navigate(new PEffect());
            }
            else
            {
                MessageBox.Show("Next step failed!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void BackClick(object p)
        {
            if (p != null)
            {
                Page np = p as Page;
                np.NavigationService.Navigate(new PImage());
            }
            else
            {
                MessageBox.Show("Next step failed!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void BrowserClick(object p)
        {
            form.OpenFileDialog dialog = new form.OpenFileDialog();
            dialog.Filter = "Text file | *.txt";
            if (dialog.ShowDialog() == form.DialogResult.OK)
            {
                TextFilePath = dialog.FileName;
            }
        }
        private void ColorSelectionChanged()
        {
            form.ColorDialog dialog = new form.ColorDialog();
            dialog.Color = TextColor;
            if(dialog.ShowDialog() == form.DialogResult.OK)
            {
                TextColor = dialog.Color;
            }
        }
    }
    
}
