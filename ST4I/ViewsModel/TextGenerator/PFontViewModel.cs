﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ST4I.Utils;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using ST4I.Views.TextGenerator.MyPage;
using ST4I.TextClass.Config;
using form = System.Windows.Forms;

namespace ST4I.ViewsModel.TextGenerator
{
    class PFontViewModel : PropertyChangedBase
    {
        public ICommand NextClickCmd { get; set; }
        public ICommand BackClickCmd { get; set; }
        public ICommand BrowserClickCmd { get; set; }
        public ICommand RemoveClickCmd { get; set; }
        public  GClass _Config = GClass.GetInstance();
        public TextClass.Config.FontStyle Style
        {
            get => _Config.Font.Style;
            set
            {
                _Config.Font.Style = value;
                OnPropertyChanged();
            }
        }
        public int FontSizeMax
        {
            get => _Config.Font.FontSize.Max;
            set
            {
                _Config.Font.FontSize.Max = value;
                OnPropertyChanged();
            }
        }
        public int FontSizeMin
        {
            get => _Config.Font.FontSize.Min;
            set
            {
                _Config.Font.FontSize.Min = value;
                OnPropertyChanged();
            }
        }
        public double OpacityMax
        {
            get => _Config.Font.Opacity.Max;
            set
            {
                _Config.Font.Opacity.Max = value;
                OnPropertyChanged();
            }
        }
        public double OpacityMin
        {
            get => _Config.Font.Opacity.Min;
            set
            {
                _Config.Font.Opacity.Min = value;
                OnPropertyChanged();
            }
        }
        public int LineSpacingMax
        {
            get => _Config.Font.LineSpacing.Max;
            set
            {
                _Config.Font.LineSpacing.Max = value;
                OnPropertyChanged();
            }
        }
        public int LineSpacingMin
        {
            get => _Config.Font.LineSpacing.Min;
            set
            {
                _Config.Font.LineSpacing.Min = value;
                OnPropertyChanged();
            }
        }
        public int LetterSpacingMax
        {
            get => _Config.Font.LetterSpacing.Max;
            set
            {
                _Config.Font.LetterSpacing.Max = value;
                OnPropertyChanged();
            }
        }
        public int LetterSpacingMin
        {
            get => _Config.Font.LetterSpacing.Min;
            set
            {
                _Config.Font.LetterSpacing.Min = value;
                OnPropertyChanged();
            }
        }
        public ObservableCollection<string> _ListFonts
        {
            get => _Config.Font.ListFonts;
            set
            {
                _Config.Font.ListFonts = value;
            }
        }

        public PFontViewModel()
        {
            NextClickCmd = new RelayCommand<object>((p) => { return true; }, (p) => { NextClick(p); });
            BackClickCmd = new RelayCommand<object>((p) => { return true; }, (p) => { BackClick(p); });
            BrowserClickCmd = new RelayCommand<object>((p) => { return true; }, (p) => { BrowserClick(p); });
            RemoveClickCmd = new RelayCommand<ListBox>((p) => RemoveFontCanExcute(p), (p) => { RemoveFont(p); });
        }
        private void NextClick(object p)
        {
            if(p != null)
            {
                Page np = p as Page;
                np.NavigationService.Navigate(new PImage());
            }
            else
            {
                MessageBox.Show("Next step failed!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void BrowserClick(object p)
        {
            if (p != null)
            {
                form.OpenFileDialog dialog = new form.OpenFileDialog();
                dialog.Multiselect = true;
                dialog.Filter = "Font files | *.ttf;*.ttc";
                if (dialog.ShowDialog() == form.DialogResult.OK)
                {
                    foreach (var item in dialog.FileNames)
                    {
                        if(!_ListFonts.Contains(item))
                            _ListFonts.Add(item);
                    }
                }
            }
            else
            {
                MessageBox.Show("Browser click failed!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private bool RemoveFontCanExcute(ListBox p)
        {
            if (p != null)
            {
                return p.SelectedIndex >= 0 && p.SelectedIndex < p.Items.Count;
            }
            return false;
        }
        private void RemoveFont(ListBox p)
        {
            if (p != null)
            {
                string fontPath = p.SelectedItem.ToString();
                _ListFonts.Remove(fontPath);
            }
            else
            {
                MessageBox.Show("Browser click failed!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void BackClick(object p)
        {
            if (p != null)
            {
                Page np = p as Page;
                np.NavigationService.Navigate(new PStart());
            }
            else
            {
                MessageBox.Show("Next step failed!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
    
}
