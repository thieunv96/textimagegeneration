﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ST4I.Utils;
using System.ComponentModel;
using System.Threading;
using System.Windows;

namespace ST4I.ViewsModel.TextGenerator
{
    class TextGeneratorViewModel : Utils.PropertyChangedBase
    {
        public ICommand CloseCmd { get; set; }
        public bool DarkTheme
        {
            get
            {
                return Properties.Settings.Default.Theme == 0;
            }
            set
            {
                Properties.Settings.Default.Theme = Convert.ToInt32(!value);
                Properties.Settings.Default.Save(); 
                if (Properties.Settings.Default.Theme == 0)
                    MessageBox.Show("Please restart program to change new theme!", "Infomation", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        public bool LightTheme
        {
            get
            {
                return Properties.Settings.Default.Theme == 1;
            }
            set
            {
                Properties.Settings.Default.Theme = Convert.ToInt32(value);
                Properties.Settings.Default.Save();
                if(Properties.Settings.Default.Theme == 1)
                MessageBox.Show("Please restart program to change new theme!", "Infomation", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
        public TextGeneratorViewModel()
        {
            CloseCmd = new RelayCommand<object>((p) => { return true; }, (p) => { CloseProgram(); });
        }
        public void CloseProgram()
        {
            foreach (Window win in Application.Current.Windows)
            {
                win.Close();
            }
        }
    }
}
