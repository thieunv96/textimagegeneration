﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ST4I.Utils;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using ST4I.Views.TextGenerator.MyPage;

namespace ST4I.ViewsModel.TextGenerator
{
    class PStartViewModel : PropertyChangedBase
    {
        public ICommand NextClickCmd { get; set; }
        public PStartViewModel()
        {
            NextClickCmd = new RelayCommand<object>((p) => { return true; }, (p) => { NextClick(p); });
        }
        private void NextClick(object p)
        {
            if(p != null)
            {
                Page np = p as Page;
                np.NavigationService.Navigate(new PFont());
            }
            else
            {
                MessageBox.Show("Next step failed!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
