﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ST4I.Utils;
using System.Threading;
using System.Windows;

namespace ST4I.ViewsModel.TextGenerator
{
    class PreviewViewModel : PropertyChangedBase
    {
        public ICommand PreviewClickCmd { get; set; }
        public PreviewViewModel()
        {
            PreviewClickCmd = new RelayCommand<object>((p) => { return true; }, (p) => { ShowPreview(p); });
        }
        private void ShowPreview(object p)
        {
            bool showNew = true;
            foreach(Window win in Application.Current.Windows)
            {
                string windowType = win.GetType().ToString();
                if (windowType.Equals("ST4I.Views.TextGenerator.PreviewWindow"))
                {
                    showNew = false;
                    win.WindowState = WindowState.Normal;
                    win.Show();
                }
            }
            if(showNew)
            {
                Views.TextGenerator.PreviewWindow previewWindow = new Views.TextGenerator.PreviewWindow();
                previewWindow.Show();
            }

        }
    }
}
