﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ST4I.Utils;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using ST4I.Views.TextGenerator.MyPage;
using ST4I.TextClass.Config;
using form = System.Windows.Forms;
using System.IO;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.Util;

namespace ST4I.ViewsModel.TextGenerator
{
    class POutputViewModel : PropertyChangedBase
    {
        public ICommand CreateClickCmd { get; set; }
        public ICommand BackClickCmd { get; set; }
        public ICommand BrowserClickCmd { get; set; }
        public  GClass _Config = GClass.GetInstance();
        public string DirSave
        {
            get => _Config.Output.DirSave;
            set
            {
                _Config.Output.DirSave = value;
                OnPropertyChanged();
            }
        }
        public TextClass.Config.NameFormat FileNameFormat
        {
            get => _Config.Output.FileNameFormat;
            set
            {
                _Config.Output.FileNameFormat = value;
                OnPropertyChanged();
            }
        }
        public TextClass.Config.LabelFileFormat LabelFormat
        {
            get => _Config.Output.LabelFormat;
            set
            {
                _Config.Output.LabelFormat = value;
                OnPropertyChanged();
            }
        }
        public POutputViewModel()
        {
            CreateClickCmd = new RelayCommand<object>((p) => { return true; }, (p) => { CreateClick(p); });
            BackClickCmd = new RelayCommand<object>((p) => { return true; }, (p) => { BackClick(p); });
            BrowserClickCmd = new RelayCommand<object>((p) => { return true; }, (p) => { BrowserClick(p); });
        }
        private void CreateClick(object p)
        {
            Views.ToolWindows.Loading loading = new Views.ToolWindows.Loading();
            loading.Status = "Load parameters and start rendering...";
            
            Thread RenderThread = new Thread(() => {
                Thread.Sleep(100);
                int numImage = _Config.Image.NumOfImage;
                bool failed = false;
                for (int i = 0; i < numImage; i++)
                {
                    Thread.Sleep(2);
                    try
                    {
                        var result = TextClass.Utils.MyAction.Create(_Config, i);

                        
                        SaveResult(result, _Config.Output, i + 1);
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                        failed = true;
                    }
                    if(failed)
                    {
                        break;
                    }
                    loading.Status = $"Rendering {i + 1}/{numImage} images...";
                }
                loading.KillWindow = true;
                if(!failed)
                MessageBox.Show("Create text images complete!", "Infomation", MessageBoxButton.OK, MessageBoxImage.Information);
            });
            RenderThread.Start();
            loading.ShowDialog();
        }
        private static void SaveResult(GResult Result, OutputCfg Output, int Id)
        {
            string savePath = Output.DirSave;
            if (!Directory.Exists(savePath))
            {
                Directory.CreateDirectory(savePath);
            }
            string path = Output.FileNameFormat == NameFormat.ByIndex ? $"{savePath}/{Id}" : $"{savePath}/{DateTime.Now.ToString($"{Id}.yyyy-MM-dd HH-mm-ss")}";
            string imagePath = $"{path}.png";
            CvInvoke.Imwrite(imagePath, Result.TextImage);
            string labelPath = string.Empty;
            switch (Output.LabelFormat)
            {
                case LabelFileFormat.Json:
                    labelPath = $"{path}.json";
                    File.WriteAllText(labelPath, Result.ToJson());
                    break;
                case LabelFileFormat.Xml:
                    labelPath = $"{path}.xml";
                    File.WriteAllText(labelPath, Result.ToXml());
                    break;
                case LabelFileFormat.TextArray:
                    labelPath = $"{path}.txt";
                    File.WriteAllText(labelPath, Result.ToTextArray());
                    break;
                case LabelFileFormat.Tesseract:
                    break;
                default:
                    break;
            }
        }
        
        private void BackClick(object p)
        {
            if (p != null)
            {
                Page np = p as Page;
                np.NavigationService.Navigate(new PEffect());
            }
            else
            {
                MessageBox.Show("Back step failed!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void BrowserClick(object p)
        {
            form.FolderBrowserDialog dialog = new form.FolderBrowserDialog();
            if(Directory.Exists(_Config.Output.DirSave))
            {
                dialog.SelectedPath = _Config.Output.DirSave;
            }
            if (dialog.ShowDialog() == form.DialogResult.OK)
            {
                DirSave = dialog.SelectedPath;
            }
        }
       
    }
    
}
