﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ST4I.Utils;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using ST4I.Views.TextGenerator.MyPage;
using ST4I.TextClass.Config;
using form = System.Windows.Forms;

namespace ST4I.ViewsModel.TextGenerator
{
    class PEffectViewModel : PropertyChangedBase
    {
        public ICommand NextClickCmd { get; set; }
        public ICommand BackClickCmd { get; set; }
        public  GClass _Config = GClass.GetInstance();
        public bool RandomTextPosition
        {
            get => _Config.Effect.RandomTextPosition;
            set
            {
                _Config.Effect.RandomTextPosition = value;
                OnPropertyChanged();
            }
        }
        public int RandomTextPositionRangeXMax
        {
            get => _Config.Effect.RandomTextPositionXRange.Max;
            set
            {
                _Config.Effect.RandomTextPositionXRange.Max = value;
                OnPropertyChanged();
            }
        }
        public int RandomTextPositionRangeXMin
        {
            get => _Config.Effect.RandomTextPositionXRange.Min;
            set
            {
                _Config.Effect.RandomTextPositionXRange.Min = value;
                OnPropertyChanged();
            }
        }
        public int RandomTextPositionRangeYMax
        {
            get => _Config.Effect.RandomTextPositionYRange.Max;
            set
            {
                _Config.Effect.RandomTextPositionYRange.Max = value;
                OnPropertyChanged();
            }
        }
        public int RandomTextPositionRangeYMin
        {
            get => _Config.Effect.RandomTextPositionYRange.Min;
            set
            {
                _Config.Effect.RandomTextPositionYRange.Min = value;
                OnPropertyChanged();
            }
        }
        public bool AddAverageBlur
        {
            get => _Config.Effect.AddAverageBlur;
            set
            {
                _Config.Effect.AddAverageBlur = value;
                OnPropertyChanged();
            }
        }
        public int AverageBlurRangeMax
        {
            get => _Config.Effect.AverageBlurRange.Max;
            set
            {
                _Config.Effect.AverageBlurRange.Max = value;
                OnPropertyChanged();
            }
        }
        public int AverageBlurRangeMin
        {
            get => _Config.Effect.AverageBlurRange.Min;
            set
            {
                _Config.Effect.AverageBlurRange.Min = value;
                OnPropertyChanged();
            }
        }
        public bool AddMedianBlur
        {
            get => _Config.Effect.AddMedianBlur;
            set
            {
                _Config.Effect.AddMedianBlur = value;
                OnPropertyChanged();
            }
        }
        public int MedianBlurRangeMax
        {
            get => _Config.Effect.MedianBlurRange.Max;
            set
            {
                _Config.Effect.MedianBlurRange.Max = value;
                OnPropertyChanged();
            }
        }
        public int MedianBlurRangeMin
        {
            get => _Config.Effect.MedianBlurRange.Min;
            set
            {
                _Config.Effect.MedianBlurRange.Min = value;
                OnPropertyChanged();
            }
        }
        public bool AddGaussianBlur
        {
            get => _Config.Effect.AddGaussianBlur;
            set
            {
                _Config.Effect.AddGaussianBlur = value;
                OnPropertyChanged();
            }
        }
        public int GaussianBlurRangeMax
        {
            get => _Config.Effect.GaussianBlurRange.Max;
            set
            {
                _Config.Effect.GaussianBlurRange.Max = value;
                OnPropertyChanged();
            }
        }
        public int GaussianBlurRangeMin
        {
            get => _Config.Effect.GaussianBlurRange.Min;
            set
            {
                _Config.Effect.GaussianBlurRange.Min = value;
                OnPropertyChanged();
            }
        }
        public bool AddPepperNoise
        {
            get => _Config.Effect.AddPepperNoise;
            set
            {
                _Config.Effect.AddPepperNoise = value;
                OnPropertyChanged();
            }
        }
        public double PepperNoiseRangeMax
        {
            get => _Config.Effect.PepperNoiseRange.Max;
            set
            {
                _Config.Effect.PepperNoiseRange.Max = value;
                OnPropertyChanged();
            }
        }
        public double PepperNoiseRangeMin
        {
            get => _Config.Effect.PepperNoiseRange.Min;
            set
            {
                _Config.Effect.PepperNoiseRange.Min = value;
                OnPropertyChanged();
            }
        }
        public PEffectViewModel()
        {
            NextClickCmd = new RelayCommand<object>((p) => { return true; }, (p) => { NextClick(p); });
            BackClickCmd = new RelayCommand<object>((p) => { return true; }, (p) => { BackClick(p); });
        }
        private void NextClick(object p)
        {
            if(p != null)
            {
                Page np = p as Page;
                np.NavigationService.Navigate(new POutput());
            }
            else
            {
                MessageBox.Show("Next step failed!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void BackClick(object p)
        {
            if (p != null)
            {
                Page np = p as Page;
                np.NavigationService.Navigate(new PText());
            }
            else
            {
                MessageBox.Show("Next step failed!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
    
}
