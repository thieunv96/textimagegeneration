﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ST4I.Utils;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using ST4I.Views.TextGenerator.MyPage;
using ST4I.TextClass.Config;
using form = System.Windows.Forms;

namespace ST4I.ViewsModel.TextGenerator
{
    class PImageViewModel : PropertyChangedBase
    {
        public ICommand NextClickCmd { get; set; }
        public ICommand BackClickCmd { get; set; }
        public ICommand BrowserClickCmd { get; set; }
        public ICommand RemoveClickCmd { get; set; }
        public ICommand ColorClickCmd { get; set; }
        public  GClass _Config = GClass.GetInstance();
        public System.Drawing.Color BGColor
        {
            get => _Config.Image.BackgroundColor;
            set
            {
                _Config.Image.BackgroundColor = value;
                OnPropertyChanged();
            }
        }
        public bool UseBGFromImage
        {
            get => _Config.Image.SetBGFromImage;
            set
            {
                _Config.Image.SetBGFromImage = value;
                OnPropertyChanged();
            }
        }
        public bool UseBGFromColor
        {
            get => !_Config.Image.SetBGFromImage;
            set
            {
                _Config.Image.SetBGFromImage = !value;
                OnPropertyChanged();
            }
        }
        public int NumOfImage
        {
            get => _Config.Image.NumOfImage;
            set
            {
                _Config.Image.NumOfImage = value;
                OnPropertyChanged();
            }
        }
        public int ImageWidth
        {
            get => _Config.Image.Width;
            set
            {
                _Config.Image.Width = value;
                OnPropertyChanged();
            }
        }
        public int ImageHeight
        {
            get => _Config.Image.Height;
            set
            {
                _Config.Image.Height = value;
                OnPropertyChanged();
            }
        }
        public double RotateXMax
        {
            get => _Config.Image.Rotation.X.Max;
            set
            {
                _Config.Image.Rotation.X.Max = value;
                OnPropertyChanged();
            }
        }
        public double RotateXMin
        {
            get => _Config.Image.Rotation.X.Min;
            set
            {
                _Config.Image.Rotation.X.Min = value;
                OnPropertyChanged();
            }
        }
        public double RotateYMax
        {
            get => _Config.Image.Rotation.Y.Max;
            set
            {
                _Config.Image.Rotation.Y.Max = value;
                OnPropertyChanged();
            }
        }
        public double RotateYMin
        {
            get => _Config.Image.Rotation.Y.Min;
            set
            {
                _Config.Image.Rotation.Y.Min = value;
                OnPropertyChanged();
            }
        }
        public double RotateZMax
        {
            get => _Config.Image.Rotation.Z.Max;
            set
            {
                _Config.Image.Rotation.Z.Max = value;
                OnPropertyChanged();
            }
        }
        public double RotateZMin
        {
            get => _Config.Image.Rotation.Z.Min;
            set
            {
                _Config.Image.Rotation.Z.Min = value;
                OnPropertyChanged();
            }
        }
        public ObservableCollection<string> ListImage
        {
            get => _Config.Image.ListBGImagePath;
            set
            {
                _Config.Image.ListBGImagePath = value;
            }
        }
        public PImageViewModel()
        {
            NextClickCmd = new RelayCommand<object>((p) => { return true; }, (p) => { NextClick(p); });
            BackClickCmd = new RelayCommand<object>((p) => { return true; }, (p) => { BackClick(p); });
            BrowserClickCmd = new RelayCommand<object>((p) => { return true; }, (p) => { BrowserClick(p); });
            RemoveClickCmd = new RelayCommand<ListBox>((p) => RemoveFontCanExcute(p), (p) => { RemoveFont(p); });
            ColorClickCmd = new RelayCommand<object>((p) => { return true; }, (p) => { ColorSelectionChanged(); });
        }
        private void NextClick(object p)
        {
            if(p != null)
            {
                Page np = p as Page;
                np.NavigationService.Navigate(new PText());
            }
            else
            {
                MessageBox.Show("Next step failed!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void BrowserClick(object p)
        {
            if (p != null)
            {
                form.OpenFileDialog dialog = new form.OpenFileDialog();
                dialog.Multiselect = true;
                dialog.Filter = "Image files | *.bmp;*.png;*.jpeg;*.jpg;*.tiff";
                if (dialog.ShowDialog() == form.DialogResult.OK)
                {
                    foreach (var item in dialog.FileNames)
                    {
                        if (!ListImage.Contains(item))
                            ListImage.Add(item);
                    }
                }
            }
            else
            {
                MessageBox.Show("Browser click failed!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private bool RemoveFontCanExcute(ListBox p)
        {
            if (p != null)
            {
                return p.SelectedIndex >= 0 && p.SelectedIndex < p.Items.Count;
            }
            return false;
        }
        private void ColorSelectionChanged()
        {
            form.ColorDialog dialog = new form.ColorDialog();
            dialog.Color = BGColor;
            if(dialog.ShowDialog() == form.DialogResult.OK)
            {
                BGColor = dialog.Color;
            }
        }
        private void RemoveFont(ListBox p)
        {
            if (p != null)
            {
                string imagePath = p.SelectedItem.ToString();
                ListImage.Remove(imagePath);
            }
            else
            {
                MessageBox.Show("Browser click failed!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void BackClick(object p)
        {
            if (p != null)
            {
                Page np = p as Page;
                np.NavigationService.Navigate(new PFont());
            }
            else
            {
                MessageBox.Show("Next step failed!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
    
}
